let roundTheMedian = true;
let roundTheStarsMedian = false;
let denyReasonCount = 6;
let defaultStarsRating = 1;

module.exports = {
	roundTheMedian,
	denyReasonCount,
	defaultStarsRating,
	roundTheStarsMedian
};
